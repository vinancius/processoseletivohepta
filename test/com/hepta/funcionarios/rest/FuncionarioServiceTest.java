package com.hepta.funcionarios.rest;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Assertions;
import com.hepta.funcionarios.dto.FuncionarioDTO;
import com.hepta.funcionarios.rest.FuncionarioService;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

class FuncionarioServiceTest {
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}
	
	@Test
	@Order(2)
	void testFuncionarioRead() {
		FuncionarioService funcionarioService = new FuncionarioService();
		
		Assertions.assertEquals(200,funcionarioService.FuncionariosRead().getStatus());
	}

	@Test
	@Order(1)
	void testFuncionarioCreate() {
		FuncionarioService funcionarioService = new FuncionarioService();
		
		FuncionarioDTO funcionario = new FuncionarioDTO("anakin",1,2000,"anakin@mail.com",21);
		Assertions.assertEquals(201,funcionarioService.FuncionarioCreate(funcionario).getStatus());
	}

	@Test
	@Order(3)
	void testFuncionarioUpdate() {
		FuncionarioService funcionarioService = new FuncionarioService();
		FuncionarioDTO funcionario = new FuncionarioDTO("Darth vader",1,66000,"DarthVader@mail.com",60);
		
		Assertions.assertEquals(200,funcionarioService.FuncionarioUpdate(1,funcionario).getStatus());

	}

	@Test
	@Order(4)
	void testFuncionarioDelete() {
		FuncionarioService funcionarioService = new FuncionarioService();
		Assertions.assertEquals(200,funcionarioService.FuncionarioDelete(1).getStatus());
	}

}
