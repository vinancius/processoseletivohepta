# Como executar?
O projeto foi desenvolvido na IDE para desenvolvimento Java chamada eclipse, assim facilitando o build e run da aplicação.
O eclipse permite escolher qual servidor web utilizar, sendo o servidor web escolhido para o projeto <strong> Tomcat v9.0 </strong>.
Após subir o servidor basta rodarmos a aplicação no server (esta ação pode ser executada pela opção <strong>"Run as" -> "run on server" </strong> do eclipse).
Para executar os teste será necessário selecionar a classe do teste desejada, logo após, selecionar opção <strong> "Run as" -> "JUnit Test" </strong>.

# Ferramentas
- MySql --> <br> &nbsp;&nbsp;
	 user: "root", <br>&nbsp;&nbsp;
	 password: "root", <br>&nbsp;&nbsp;
	 connection name: "funcionarios-bd", <br>&nbsp;&nbsp;
	 schema: "Funcionarios_prova"<br>
<br>	 
- Eclipse 2021‑09
- Java 
- JDK-11
- Tomcat v9.0