function doPost() {
    var nome = document.getElementById('nome').value;

	if(nome != "") {
		axios.post("/funcionarios/rs/setor",
		{
			nome: nome,
		},
	    {
			headers: { 'Content-Type': 'application/json; charset=UTF-8' }
		})
	    .then(function (response) {
	        window.location.href = "/funcionarios/index.html";
			alert("Cadastrado com succeso!");
	    })
	    .catch(function (response) {
	        console.log(response);
	    });
	}
};