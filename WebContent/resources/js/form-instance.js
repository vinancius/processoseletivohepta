var form = new Vue({
	el:"#form",
    data: {
        setores: [],
		funcionario: {
			id: null,
			nome: "",
			idade: null,
			salario: null,
			email: "",
			setor: null,
		}
    },
    created: function(){
        let vm =  this;
		const urlParams = new URLSearchParams(window.location.search);
		
        vm.listarSetores();
		if(urlParams.get('id') != null) {
			vm.getFuncionario(urlParams);
		}
    },
    methods:{
        listarSetores: function(){
			const vm = this;
			axios.get("/funcionarios/rs/setor/")
			.then(response => {vm.setores = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		getFuncionario: function(urlParams){
			const vm = this;
			vm.funcionario.id = urlParams.get('id');
			vm.funcionario.nome = urlParams.get('nome');
			vm.funcionario.salario = urlParams.get('salario');
			vm.funcionario.email = urlParams.get('email');
			vm.funcionario.setor = urlParams.get('setor');
			vm.funcionario.idade = urlParams.get('idade');
		},
		doEdit(id) {
			var nome = document.getElementById('nome').value;
			var setor = document.getElementById('setorInput');
			var email = document.getElementById('email').value;
			var salario = document.getElementById('salario').value;
			var idade = document.getElementById('idade').value;
			if((nome && email != "") && (setor && idade && salario != 0)) {
			    axios.put(`/funcionarios/rs/funcionarios/${id}`,
				{
					nome: nome,
					setor: parseInt(setor.value),
					salario: parseFloat(salario),
					email: email,
					idade: parseInt(idade)
				},
			    {
					headers: { 'Content-Type': 'application/json; charset=UTF-8' }
				})
			    .then(function (response) {
					window.location.href = "/funcionarios/index.html";
					alert("Atualizado com succeso!");
			    })
			    .catch(function (response) {
			        console.log(response);
					alert("pause");
			    });
			};
		}
    }
});