function doPost() {
    var nome = document.getElementById('nome').value;
	var setor = document.getElementById('setorInput');
	var email = document.getElementById('email').value;
	var salario = document.getElementById('salario').value;
	var idade = document.getElementById('idade').value;
	if((nome && email != "") && (setor && idade && salario != 0)) {
		axios.post("/funcionarios/rs/funcionarios",
		{
			nome: nome,
			setor: parseInt(setor.value),
			salario: parseFloat(salario),
			email: email,
			idade: parseInt(idade)
		},
	    {
			headers: { 'Content-Type': 'application/json; charset=UTF-8' }
		})
	    .then(function (response) {
	        window.location.href = "/funcionarios/index.html";
			alert("Cadastrado com succeso!");
	    })
	    .catch(function (response) {
	        console.log(response);
	    });
	}
};