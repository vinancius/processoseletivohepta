var inicio = new Vue({
	el:"#inicio",
    data: {
        lista: []
    },
    created: function(){
        let vm =  this;
        vm.listarFuncionarios();
    },
    methods:{
	//Busca os itens para a lista da primeira página
        listarFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios")
			.then(response => {vm.lista = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		doDelete: function (id) {
			axios.delete(`/funcionarios/rs/funcionarios/${id}`)
		    .then(response => {
		      window.location.reload ()
		    }).catch(function (response) {
		        console.log(response);
		    });
		},
		redirectToEdit: function(funcionario) {
			window.location.href = `/funcionarios/pages/editar-funcionario.html
				?id=${funcionario.id}&nome=${funcionario.nome}&idade=${funcionario.idade}
				&salario=${funcionario.salario}&email=${funcionario.email}&setor=${funcionario.setor.id}`;
		}
    }
});