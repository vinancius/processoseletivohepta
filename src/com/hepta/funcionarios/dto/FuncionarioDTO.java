package com.hepta.funcionarios.dto;
import com.hepta.funcionarios.entity.Funcionario;

public class FuncionarioDTO {
	private String nome;
	private int setor;
	private double salario;
	private String email;
	private int idade;
	
	public FuncionarioDTO() {

	}
	
	public FuncionarioDTO(String nome, int setor, double salario, String email, int idade) {
		super();
		this.nome = nome;
		this.setor = setor;
		this.salario = salario;
		this.email = email;
		this.idade = idade;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getSetor() {
		return setor;
	}
	public void setSetor(int setor) {
		this.setor = setor;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	public Funcionario convertDtoToEntity() {
		Funcionario funcionario = new Funcionario();
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setSalario(salario);
		funcionario.setIdade(idade);
		return funcionario;
	}
	
}
